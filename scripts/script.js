let popupYoutube = document.querySelector(".popup-youtube");
let closePopup = document.querySelector(".close");
let playBtn = document.querySelector(".play");
let iframe = document.querySelector("iframe");

// handle opening and closing youtube popup
playBtn.addEventListener("click", function() {
    popupYoutube.style.display = "block";
    iframe.setAttribute("src", "https://www.youtube.com/embed/tvaP-X2yCDQ?controls=0&autoplay=1");
});

closePopup.addEventListener("click", function() {
    popupYoutube.style.display = "none";        
    iframe.setAttribute("src", "");
});

popupYoutube.addEventListener("click", function() {
    popupYoutube.style.display = "none";
    iframe.setAttribute("src", "");
})